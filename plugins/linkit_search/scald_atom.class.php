<?php
/**
 * @file
 * Define a Linkit search plugin class just for Scald Files.
 */

/**
 * The Linkit search plugin.
 */
class LinkitSearchPluginScaldFile extends LinkitSearchPluginEntity {

  /**
   * Overrides LinkitSearchPluginEntity::createPath().
   */
  function createPath($entity) {
    $path = '';

    if (!empty($entity->provider)) {
      switch ($entity->provider) {
        case 'scald_file':
          $file_uri = field_get_items('scald_atom', $entity, 'scald_file');
          if ($file_uri && $this->conf['file_link_type'] == 'direct') {
            $path = file_create_url($file_uri[0]['uri']);
          }
          break;
        case 'scald_image':
          $image_uri = field_get_items('scald_atom', $entity, 'scald_thumbnail');
          if ($image_uri && $this->conf['image_link_type'] == 'direct') {
            $path = file_create_url($image_uri[0]['uri']);
          }
          break;
        case 'scald_video':
          $video_uri = !empty($entity->data['video_file']) ? $entity->data['video_file'] : FALSE;
          if ($video_uri && $this->conf['video_link_type'] == 'direct') {
            $path = file_create_url($video_uri);
          }
          break;
        case 'scald_vimeo':
          if (!empty($entity->base_id) &&  $this->conf['vimeo_link_type'] == 'direct') {
            $path = 'https://vimeo.com/' . $entity->base_id;
          }
          break;
        case 'scald_youtube':
          if (!empty($entity->base_id) && $this->conf['youtube_link_type'] == 'direct') {
            $path = 'https://www.youtube.com/watch?v=' . $entity->base_id;
          }
          break;
      }
    }

    if ($path) {
      // There has got to be a better way to ensure that for local filepaths,
      // we use absolute paths instead of full URIs.
      global $base_url;
      if (strpos($path, $base_url) === 0) {
        $path = substr($path, strlen($base_url));
      }

      return linkit_get_insert_plugin_processed_path($this->profile, $path, array('language' => (object) array('language' => FALSE)));
    }

    return parent::createPath($entity);
  }

  /**
   * Overrides LinkitSearchPluginEntity::buildSettingsForm().
   */
  function buildSettingsForm() {
    $form = parent::buildSettingsForm();

    if (module_exists('scald_file')) {
      $form['entity:scald_atom']['file_link_type'] = array(
        '#title' => t('When linking to a file:'),
        '#type' => 'radios',
        '#options' => array(
          'direct' => t('Direct file link'),
          'entity' => t('Entity detail page'),
        ),
        '#default_value' => isset($this->conf['file_link_type']) ? $this->conf['file_link_type'] : 'direct',
      );
    }

    if (module_exists('scald_image')) {
      $form['entity:scald_atom']['image_link_type'] = array(
        '#title' => t('When linking to an image:'),
        '#type' => 'radios',
        '#options' => array(
          'direct' => t('Direct file link'),
          'entity' => t('Entity detail page'),
        ),
        '#default_value' => isset($this->conf['image_link_type']) ? $this->conf['image_link_type'] : 'direct',
      );
    }

    if (module_exists('scald_video')) {
      $form['entity:scald_atom']['video_link_type'] = array(
        '#title' => t('When linking to an uploaded video file:'),
        '#type' => 'radios',
        '#options' => array(
          'direct' => t('Direct link to primary video file'),
          'entity' => t('Entity detail page'),
        ),
        '#default_value' => isset($this->conf['video_link_type']) ? $this->conf['video_link_type'] : 'direct',
      );
    }

    if (module_exists('scald_youtube')) {
      $form['entity:scald_atom']['youtube_link_type'] = array(
        '#title' => t('When linking to a YouTube video:'),
        '#type' => 'radios',
        '#options' => array(
          'direct' => t('Direct link to the video\'s page on YouTube'),
          'entity' => t('Entity detail page'),
        ),
        '#default_value' => isset($this->conf['youtube_link_type']) ? $this->conf['youtube_link_type'] : 'direct',
      );
    }

    if (module_exists('scald_vimeo')) {
      $form['entity:scald_atom']['vimeo_link_type'] = array(
        '#title' => t('When linking to a Vimeo video:'),
        '#type' => 'radios',
        '#options' => array(
          'direct' => t('Direct link to the video\'s page on Vimeo'),
          'entity' => t('Entity detail page'),
        ),
        '#default_value' => isset($this->conf['vimeo_link_type']) ? $this->conf['vimeo_link_type'] : 'direct',
      );
    }


    return $form;
  }
}
